import { LightningElement, track, api } from 'lwc';

export default class toast extends LightningElement {
    @api type;
    @api message;

    @track isSuccess = false;
    @track isError = false;

    connectedCallback() {
        if (this.type == 'success') {
            this.isSuccess = true;
            this.isError = false;
        } else if (this.type == 'error') {
            this.isSuccess = false;
            this.isError = true;
        }
    }
    closeToast(){
        this.isError = false;
        this.isSuccess = false;
    }
}